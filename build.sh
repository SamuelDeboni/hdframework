comon_flags="-msse2"
flags="-lopenal -lglfw -lGLEW -lGL -lm"
compiler="clang++"
#compiler="zig c++"
std_version='c++14'
input=src/moons.cc
output=game

[ "$1" = "run" ] && rm $output

time $compiler $input -std=$std_version -o $output $comon_flags $flags

[ "$1" = "run" ] &&./$output
