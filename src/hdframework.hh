#ifndef HDFRAMEWORK_H
#define HDFRAMEWORK_H

// TODO: Maybe make this c compatible, but I don't know is worth

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdarg.h>
#include <stdint.h>
#include <assert.h>

#include <GL/glew.h>
 
#include <GLFW/glfw3.h>

#include "hdf_math.hh"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#define STBI_NO_FAILURE_STRINGS
#include "libs/stb_image.h"

#include "libs/ifr_sound.h"

#define hd_assert(x) assert((x))

typedef int32_t bool32;

// ------ hdf logging system --------------------------------------------------
// NOTE: Maybe put the log stuff in other file
// TODO: Log in parallel
FILE* hdf_Log_File;		// Main log file

internal FILE*
hdf_open_log(const char* log_foder, const char* log_name)
{
	FILE* file;
	time_t t;
	struct tm* local_time;

	time(&t);
	local_time = localtime(&t);

	size_t lf_size = strlen(log_foder) + 1;

	size_t ln_size = strlen(log_name) + 1;

	char *fn = (char*)calloc(256, sizeof(char));
	char *ts = (char*)calloc(256, sizeof(char));
	strftime(ts, 32, "%F_%T_", local_time);
	strncpy(fn, log_foder, lf_size);
	strncat(fn, "/", 2);
	strncat(fn, ts, 32);
	strncat(fn, log_name , ln_size);
	strncat(fn, ".log", 5);

	file = fopen(fn, "w");
	hd_assert(file);
	free(fn);
	free(ts);
	return file;
}

internal FILE*
hdf_open_log_const_name(const char* log_path)
{
	FILE* file;
	time_t t;
	
	file = fopen(log_path, "w");
	hd_assert(file);
	return file;
}

internal void
hdf_log(FILE* log_file, const char* format, ...)
{
	if (log_file == NULL)
		return;

	time_t t;
	struct tm* local_time;

	time(&t);
	local_time = localtime(&t);

	char ts[32];
	strftime(ts, 32, "[%T] ", local_time);

	char buffer[256];
	va_list args;
	va_start (args, format);
	vsprintf (buffer,format, args);
	fprintf(log_file, "%s %s\n",ts, buffer);
	va_end(args);
}

internal void
hdf_log(const char* format, ...)
{
	hd_assert(hdf_Log_File != NULL);
	time_t t;
	struct tm* local_time;

	time(&t);
	local_time = localtime(&t);

	char ts[32];
	strftime(ts, 32, "[%T] ", local_time);

	char buffer[256];
	va_list args;
	va_start (args, format);
	vsprintf (buffer,format, args);
	fprintf(hdf_Log_File, "%s %s\n",ts, buffer);
	va_end(args);
}


internal void
hdf_log_err(FILE* log_file, const char* format, ...)
{
	if (log_file == NULL)
		return;
	time_t t;
	struct tm* local_time;

	time(&t);
	local_time = localtime(&t);

	char ts[32];
	strftime(ts, 32, "[%T] ERROR: ", local_time);

	char buffer[256];
	va_list args;
	va_start (args, format);
	vsprintf (buffer,format, args);
	fprintf(log_file, "%s %s\n",ts, buffer);
	fprintf(stderr, "%s %s\n",ts, buffer);
	va_end(args);
	exit(0);
}

internal void
hdf_log_err(const char* format, ...)
{
	hd_assert(hdf_Log_File != NULL);
	time_t t;
	struct tm* local_time;

	time(&t);
	local_time = localtime(&t);

	char ts[32];
	strftime(ts, 32, "[%T] ERROR: ", local_time);

	char buffer[256];
	va_list args;
	va_start (args, format);
	vsprintf (buffer,format, args);
	fprintf(hdf_Log_File, "%s %s\n",ts, buffer);
	fprintf(stderr, "%s %s\n",ts, buffer);
	va_end(args);
	exit(0);
}

internal inline u32
POWB_2(i32 x) {
	if (x > 0) return (u32)2 << ((i32)x-1); else return 1;
}

#include "input_config.hh"

// --- Inputs -------------------------------------------------------------
u32 hdf_Input_Pressed = 0;
u32 hdf_Input_Up = 0;
u32 hdf_Input_Down = 0;
internal inline bool
hdf_get_input(u32 input_mask, u32 input) { return (input_mask & input) == input; }

// ------------------------------------------------------------------------

// --- Structs -------------------------------------------------------------

// Holds all the important information of a WAV audio stream
struct hdf_Audio {
	ifr_SoundBuffer buffer;
	ifr_SoundSpeaker speaker;
};

// Basic struct for vertices
struct hdf_Vertices {
	Vec3 position;
	Vec3 normal;
	Vec2 uv_cordinates;
};

/*
struct hdf_Gl_Buffers {
	GLuint vao;	// Vertex arrey object
	GLuint vb;	// Vertex buffer
	GLuint eb;	// Element buffer
	i32 e_count; // Element count, needed for the glRenderTriangles function
};
*/
struct hdf_Sprite {
	GLuint vao;	// Vertex arrey object
	GLuint vb;	// Vertex buffer
	GLuint eb;	// Element buffer

	GLuint texture;	// Opengl texture
	GLuint shader;	// Shader program
	f32 x0, y0, x1, y1;
};

// Basic opengl mesh
struct hdf_Mesh {
	GLuint vao;	// Vertex arrey object
	GLuint vb;	// Vertex buffer
	GLuint eb;	// Element buffer
	i32 e_count; // Element count, needed for the glRenderTriangles function

	GLuint texture;	// Opengl texture
	GLuint shader;	// Shader program
};

// A opengl mesh for instance draw
struct hdf_Instance_Mesh {
	GLuint vao;	// Vertex arrey object
	GLuint vb;	// Vertex buffer
	GLuint eb;	// Element buffer
	i32 e_count; // Element count, needed for the glRenderTriangles function

	GLuint texture;	// Opengl texture
	GLuint shader;	// Shader program
	
	GLuint mb; // Model matrix buffer
	GLuint nb; // Normal Matrix buffer
};

// 3d transform struct
struct hdf_Transform {
	Vec3 position;
	Vec3 scale;
	Vec4 rotation;
};

struct hdf_Model
{
	hdf_Mesh mesh;
	hdf_Transform transform;
};

struct hdf_Camera {
	hdf_Transform transform;
	Mat4 projection_Matrix;
	Mat4 cam_matrix;
};

// A 4 bit RGBA Ray image
struct hdf_Image {
	i32 width;
	i32 height;
	i32 depth;
	u8 *data;
};

// --- Global Variables ---------------------------------------------------
GLFWwindow* 	hdf_Main_Window;			// The main glfw window
Vec2i 			hdf_Window_Size;			// Main window sizw

//NOTE: camera needs more thinking
hdf_Camera 		hdf_Main_Camera;			// Main 2d camera
Mat4 			hdf_Ortogonal_Matrix;		// Ortogona matrix
Mat4 			hdf_Perpective_Matrix;		// Ortogona matrix
hdf_Mesh 		hdf_Sprite_Mesh;			// A sprite mesh, it's just a quad
GLuint 			hdf_Sprite_Shader_Program;	// Default shader for sprites
GLuint 			hdf_Default_Shader_Program;	// Default shader for sprites

// Start function callback
// It is executed once after initialization
internal void (*start_func)();

// Update and renderer function callback
// It is executed once a frame after glClean and before swap windows
internal void (*update_and_renderer_func)(f32);
// ------------------------------------------------------------------------

// --- Functions declaration ----------------------------------------------

// Initialization function
internal void hdf_init(Vec2i window_size, void (*start_func)(), void (*update_and_renderer_func)(f32));
internal void hdf_quit(GLFWwindow* window);

internal inline void hdf_load_audio(hdf_Audio *audio, const char* fn);
internal inline void hdf_destroy_audio(hdf_Audio audio);

internal inline void hdf_play_audio(hdf_Audio audio);
internal inline void hdf_pause_audio(hdf_Audio audio);

internal char* hdf_load_text_file(const char* fn);

internal GLuint hdf_gltexture_from_image(hdf_Image image);

internal inline hdf_Image hdf_allocate_image(u32 width, u32 height, u32 depth);
internal inline void hdf_destroy_image(hdf_Image image);
internal hdf_Image hdf_load_png_file(const char* fn);

internal GLuint hdf_load_texture_from_file(const char* fn);
internal inline void hdf_destroy_texture(GLuint texture);

internal inline void hdf_render_mesh(hdf_Mesh mesh, hdf_Transform transform);
internal inline void hdf_render_mesh_instances(hdf_Mesh mesh, hdf_Transform *transform, 
		Mat4 *model_matrices, Mat3 *normal_matrices, u32 count);
internal inline void hdf_render_mesh_modulated(hdf_Mesh mesh, hdf_Transform *transform, Vec3 *colors, u32 count);

internal GLuint hdf_compile_shader(const char* source, GLenum type);
internal GLuint hdf_create_shader_program(char* vertex_source, char* fragment_source);

internal hdf_Mesh
hdf_create_mesh(hdf_Vertices vertices[], u32 elements[],
			i32 vertice_size, i32 element_size,
			GLuint shader, GLuint texture);

internal hdf_Instance_Mesh 
hdf_create_instance_mesh(hdf_Vertices vertices[], i32 elements[], i32 vertice_size,
	i32 element_size, GLuint shader, GLuint texture);

internal hdf_Instance_Mesh hdf_create_instance_mesh(hdf_Mesh mesh);

internal inline void hdf_destroy_mesh(hdf_Mesh mesh);

internal inline Mat4 hdf_create_ortogonal_matrix(f32 n, f32 f, f32 r, f32 t, f32 s);
internal inline Mat4 hdf_create_projection_matrix(f32 n, f32 f, f32 r, f32 t);

internal hdf_Mesh hdf_createMeshOBJ(const char* filename, GLuint shader);

// ------------------------------------------------------------------------

internal void
hdf_glfw_error_callback(int error, const char* description)
{
	hdf_log_err(hdf_Log_File, "GLFW ERROR %d\n%s", error, description);
}

internal void
hdf_input_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	// Set one time inputs to 0
	hdf_Input_Down = 0;
	hdf_Input_Up = 0;
	switch (action)
	{
		case GLFW_PRESS:
		{
			for (u32 i = 0; i < INPUT_LEN; i++)
				if (key == Key_Map[i])
				{
					if ((hdf_Input_Pressed & POWB_2(i)) != POWB_2(i))
						hdf_Input_Down |= POWB_2(i);
					hdf_Input_Pressed |= POWB_2(i);
				}
		} break;

		case GLFW_RELEASE:
		{
			for (u32 i = 0; i < INPUT_LEN; i++)
				if (key == Key_Map[i])
				{
					if ((hdf_Input_Pressed & POWB_2(i)) == POWB_2(i))
						hdf_Input_Up |= POWB_2(i);
					hdf_Input_Pressed &= ~POWB_2(i);
				}
		} break;
	}
}

internal void
hdf_init(Vec2i window_size, void (*start_callback)(), void (*update_and_renderer_calkback)(f32))
{
	hdf_Log_File = hdf_open_log_const_name("Logs/hdf_last.log");
	hdf_log(hdf_Log_File, "Starting GLFW");

	// Initialize SDL Modules
	glfwSetErrorCallback(hdf_glfw_error_callback);
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_SAMPLES, 2);

	hdf_log(hdf_Log_File, "GLFW initialized with success");

	hdf_log(hdf_Log_File, "Setting Callbacks");
	// Set the callback functions
	start_func = start_callback;
	update_and_renderer_func = update_and_renderer_calkback;

	// Create the main window
	hdf_log(hdf_Log_File, "Creating window");
	hdf_Main_Window = glfwCreateWindow(
		window_size.x, window_size.y,
		"Title", NULL, NULL);
	hdf_Window_Size = window_size;

	glfwSetKeyCallback(hdf_Main_Window, hdf_input_callback);

	// Create opengl context
	glfwMakeContextCurrent(hdf_Main_Window);
	glewExperimental = GL_TRUE;
	glewInit();

	ifr_initAudioLib();

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable( GL_BLEND );
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

#if 0
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);  
#endif

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// Compiling basic shaders ---------------------------------------------------
	{ // NOTE: Maybe move this to a better place
		hdf_log(hdf_Log_File, "Compiling shaders");
		char* vertex_source = hdf_load_text_file("Shaders/sprite.vert");
		char* fragment_source = hdf_load_text_file("Shaders/sprite.frag");
		hdf_Sprite_Shader_Program = hdf_create_shader_program(vertex_source, fragment_source);

		free(vertex_source);
		free(fragment_source);

		vertex_source = hdf_load_text_file("Shaders/default.vert");
		fragment_source = hdf_load_text_file("Shaders/default.frag");
		hdf_Default_Shader_Program = hdf_create_shader_program(vertex_source, fragment_source);

		free(vertex_source);
		free(fragment_source);

		// Sptite mesh creation
		hdf_Vertices sprite_vertices[] = {
			{{-0.5, -0.5, 0.0}, {0.0, 0.0, 0.0}, {0.0, 1.0}},
			{{ 0.5, -0.5, 0.0}, {0.0, 0.0, 0.0}, {1.0, 1.0}},
			{{ 0.5,  0.5, 0.0}, {0.0, 0.0, 0.0}, {1.0, 0.0}},
			{{-0.5,  0.5, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0}},
		};

		u32 elements[] = {0,1,3,1,2,3};

		hdf_Sprite_Mesh = hdf_create_mesh(sprite_vertices, elements, 4 * sizeof(hdf_Vertices), 6 * sizeof(i32), hdf_Sprite_Shader_Program, 0);
	}
	// ---------------------------------------------------------------------------

	hdf_Ortogonal_Matrix = hdf_create_ortogonal_matrix(0.2f, 1000.0f, ((f32)hdf_Window_Size.x/(f32)hdf_Window_Size.y) *  0.1f, 0.1f, 0.02f);
	hdf_Perpective_Matrix = hdf_create_projection_matrix(0.2f, 1000.0f, ((f32)hdf_Window_Size.x/(f32)hdf_Window_Size.y) *  0.1f, 0.1f);

	hdf_Main_Camera.transform.position = (Vec3){0,0,20};
	hdf_Main_Camera.transform.scale = (Vec3){1,1,1};
	hdf_Main_Camera.transform.rotation = euler_to_quat((Vec3){0,0,0});
	hdf_Main_Camera.projection_Matrix = hdf_Perpective_Matrix;

	// Call the start function
	hdf_log(hdf_Log_File, "Calling start funtion");
	start_func();

	// --- Main game loop -------------------------------------------------
	f32 delta = 0; // Delta time between frames
	f32 itime = glfwGetTime(); // Start time of the frame
	f32 ltime = 0;

	hdf_log(hdf_Log_File, "Main loop start");
	uint64_t cy;
	while (!glfwWindowShouldClose(hdf_Main_Window))
	{
		ltime = itime;
		itime = glfwGetTime(); // Get the initial frame time
		delta = itime - ltime;
		if (delta > 0.1f) // Prevent delta to go to large in case of lag
			delta = 0.1f;

		// Update main camera matrix
		Mat4 cam_pos = (Mat4) {
			hdf_Main_Camera.transform.scale.x, 0.0f, 0.0f, -hdf_Main_Camera.transform.position.x,
				0.0f, hdf_Main_Camera.transform.scale.y, 0.0f, -hdf_Main_Camera.transform.position.y,
				0.0f, 0.0f, hdf_Main_Camera.transform.scale.z, -hdf_Main_Camera.transform.position.z,
				0.0f, 0.0f, 0.0f, 1.0f,
		};
		Mat4 cam_rot = mat4_from_quaternion(hdf_Main_Camera.transform.rotation);
		hdf_Main_Camera.cam_matrix = mat4_mult(&cam_rot ,&cam_pos);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		update_and_renderer_func(delta); // Call the update function
		glfwSwapBuffers(hdf_Main_Window);
		glfwPollEvents();
	}
}

internal void
hdf_quit()
{
	hdf_destroy_mesh(hdf_Sprite_Mesh);
	glDeleteProgram(hdf_Sprite_Shader_Program);

	hdf_log(hdf_Log_File, "Exiting");
	fclose(hdf_Log_File);
	glfwDestroyWindow(hdf_Main_Window);
	glfwTerminate();
}

internal inline void
hdf_load_audio(hdf_Audio *audio, const char* fn)
{
	ifr_initSpeaker(&audio->speaker, AL_TRUE);
	ifr_loadOGGAudioBuffers((char**)&fn, 1, &audio->buffer);
	ifr_loadSongToSpeaker(audio->speaker,  audio->buffer);
}

internal inline void
hdf_destroy_audio(hdf_Audio audio)
{
	ifr_deleteAudioBuffers(0,1, &audio.buffer);
	ifr_deleteSpeaker(audio.speaker);
}

internal inline void
hdf_play_audio(hdf_Audio audio)
{
	ifr_playSpeaker(audio.speaker);
}

internal inline void
hdf_pause_audio(hdf_Audio audio) 
{
	ifr_pauseSpeaker(audio.speaker);
}

internal char*
hdf_load_text_file(const char* fn)
{
	FILE* f = fopen(fn, "rb");
	if (f == NULL)
		return NULL;
	fseek(f, 0, SEEK_END);
	long size = ftell(f) + 1;
	fseek(f, 0, SEEK_SET);
	char* ret = (char*)calloc(size + 1, sizeof(char));
	fread(ret, size, 1, f);
	fclose(f);
	return ret;
}

// ----- OpenGL and rendering in general --------------------------------------
internal GLuint
hdf_gltexture_from_image(hdf_Image image)
{
	GLuint tex_id;
	glGenTextures(1, &tex_id);
	glBindTexture(GL_TEXTURE_2D, tex_id);
	int Mode = GL_RGB;

	if(image.depth == 4)
		Mode = GL_RGBA;

	glTexImage2D(GL_TEXTURE_2D, 0, Mode, image.width, image.height, 0, Mode, GL_UNSIGNED_BYTE, image.data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);

	return tex_id;
}

internal inline hdf_Image 
hdf_allocate_image(u32 width, u32 height, u32 depth)
{
	hdf_Image image;

	hd_assert(width > 0);
	hd_assert(height > 0);

	image.width = width;
	image.height = height;
	image.depth = depth;
	image.data = (u8 *)malloc(width*height*depth * sizeof(u8));
	return image;
}

internal inline void 
hdf_destroy_image(hdf_Image image)
{
	free(image.data);
}

internal hdf_Image
hdf_load_png_file(const char* fn)
{
	hdf_Image image;
	image.data = stbi_load(fn, &image.width, &image.height, &image.depth, 0);
	hd_assert(image.data != NULL);
	return image;
}

internal GLuint
hdf_load_texture_from_file(const char* fn)
{
	hdf_Image image = hdf_load_png_file(fn);
	hdf_log(hdf_Log_File, "loading image %s", fn);

	GLuint tex_id;
	glGenTextures(1, &tex_id);
	glBindTexture(GL_TEXTURE_2D, tex_id);
	i32 Mode = GL_RGB;

	if(image.depth == 4) {
		hdf_log(hdf_Log_File, "Surface is rgba");
		Mode = GL_RGBA;
	}
	else if(image.depth == 3)
		hdf_log(hdf_Log_File, "Surface is rgb");

	glTexImage2D(GL_TEXTURE_2D, 0, Mode, image.width, image.height, 0, Mode, GL_UNSIGNED_BYTE, image.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	hdf_destroy_image(image);

	return tex_id;
}

internal inline void
hdf_destroy_texture(GLuint texture)
{
	glDeleteTextures(1, &texture);
}

internal void
hdf_render_sprite(hdf_Sprite sprite, hdf_Transform transform)
{
	glBindVertexArray(sprite.vao);
	glEnableVertexAttribArray(sprite.vao);
	glUseProgram(sprite.shader);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, sprite.texture);
	glUniform1i(glGetUniformLocation (sprite.shader, "tex"), 0);

	glUniformMatrix4fv (glGetUniformLocation (sprite.shader, "projectionMatrix"),
			1, GL_FALSE, (f32*)(&hdf_Main_Camera.projection_Matrix));

	glUniformMatrix4fv (glGetUniformLocation (sprite.shader, "cameraMatrix"),
			1, GL_FALSE, (f32*)(&hdf_Main_Camera.cam_matrix));

	glUniform4f(glGetUniformLocation (sprite.shader, "colorMod"), 1, 1, 1, 1);

	// Model matrix
	Mat3 rot_m = mat3_from_quaternion(transform.rotation);
	Mat3 scale_m = (Mat3) {
		transform.scale.x, 0.0f, 0.0f,
        0.0f, transform.scale.y, 0.0f,
        0.0f, 0.0f, transform.scale.z
	};
	rot_m = mat3_mult(&rot_m, &scale_m);

	Mat4 model_m = (Mat4) {
		rot_m.x.x, rot_m.x.y, rot_m.x.z, transform.position.x,
		rot_m.y.x, rot_m.y.y, rot_m.y.z, transform.position.y,
		rot_m.z.x, rot_m.z.y, rot_m.z.z, transform.position.z,
		0.0f, 0.0f, 0.0f, 1.0f,
	};

	glUniformMatrix4fv (glGetUniformLocation (sprite.shader, "modelViewMatrix"),
			1, GL_FALSE, (f32*)(&model_m));

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

internal void
hdf_render_mesh(hdf_Mesh mesh, hdf_Transform transform)
{
	glBindVertexArray(mesh.vao);
	glUseProgram(mesh.shader);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh.texture);
	glUniform1i(glGetUniformLocation (mesh.shader, "tex"), 0);

	glUniformMatrix4fv (glGetUniformLocation (mesh.shader, "projectionMatrix"),
			1, GL_FALSE, (f32*)(&hdf_Main_Camera.projection_Matrix));

	glUniformMatrix4fv (glGetUniformLocation (mesh.shader, "cameraMatrix"),
			1, GL_FALSE, (f32*)(&hdf_Main_Camera.cam_matrix));

	glUniform3f(glGetUniformLocation (mesh.shader, "colorMod"), 1, 1, 1);

	// Model matrix
	Mat3 rot_m = mat3_from_quaternion(transform.rotation);
	Mat3 scale_m = (Mat3) {
		transform.scale.x, 0.0f, 0.0f,
        0.0f, transform.scale.y, 0.0f,
        0.0f, 0.0f, transform.scale.z
	};
	rot_m = mat3_mult(&rot_m, &scale_m);

	Mat4 model_m = (Mat4) {
		rot_m.x.x, rot_m.x.y, rot_m.x.z, transform.position.x,
		rot_m.y.x, rot_m.y.y, rot_m.y.z, transform.position.y,
		rot_m.z.x, rot_m.z.y, rot_m.z.z, transform.position.z,
		0.0f, 0.0f, 0.0f, 1.0f,
	};

	glUniformMatrix4fv (glGetUniformLocation (mesh.shader, "modelViewMatrix"),
			1, GL_FALSE, (f32*)(&model_m));

	// Normal matrix transposed
	Mat3 normal_matrix = (Mat3){
		model_m.e[0][0], model_m.e[1][0], model_m.e[2][0],
		model_m.e[0][1], model_m.e[1][1], model_m.e[2][1],
		model_m.e[0][2], model_m.e[1][2], model_m.e[2][2],
	};
	glUniformMatrix3fv (glGetUniformLocation (mesh.shader, "normalMatrix"),
		1, GL_FALSE, (f32*)(&normal_matrix));

	glDrawElements(GL_TRIANGLES, mesh.e_count, GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);

}

internal void
hdf_render_mesh_instances(hdf_Instance_Mesh mesh, hdf_Transform *transform,
		Mat4 *model_matrices, Mat3 *normal_matrices,u32 count)
{
	glBindVertexArray(mesh.vao);
	glUseProgram(mesh.shader);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh.texture);
	glUniform1i(glGetUniformLocation (mesh.shader, "tex"), 0);

	glUniformMatrix4fv (glGetUniformLocation (mesh.shader, "projectionMatrix"),
			1, GL_FALSE, (f32*)(&hdf_Main_Camera.projection_Matrix));

	glUniformMatrix4fv (glGetUniformLocation (mesh.shader, "cameraMatrix"),
			1, GL_FALSE, (f32*)(&hdf_Main_Camera.cam_matrix));

	glUniform3f(glGetUniformLocation (mesh.shader, "colorMod"), 1, 1, 1);


	hdf_Transform *transf = transform;
	for(u32 i = 0; i < count; i++)
	{
		// Model matrix
		Mat3 rot_m = mat3_from_quaternion(transf->rotation);
		Mat3 scale_m = (Mat3) {
			transf->scale.x, 0.0f, 0.0f,
			0.0f, transf->scale.y, 0.0f,
			0.0f, 0.0f, transf->scale.z
		};
		rot_m = mat3_mult(&rot_m, &scale_m);

		Mat4 model_m = (Mat4) {
			rot_m.x.x, rot_m.x.y, rot_m.x.z, transf->position.x,
			rot_m.y.x, rot_m.y.y, rot_m.y.z, transf->position.y,
			rot_m.z.x, rot_m.z.y, rot_m.z.z, transf->position.z,
			0.0f, 0.0f, 0.0f, 1.0f,
		};

		model_matrices[i] = model_m;

		// Normal matrix transposed
		Mat3 normal_matrix = (Mat3){
			model_m.e[0][0], model_m.e[1][0], model_m.e[2][0],
			model_m.e[0][1], model_m.e[1][1], model_m.e[2][1],
			model_m.e[0][2], model_m.e[1][2], model_m.e[2][2],
		};

		normal_matrices[i] = normal_matrix;
		transf++;
	}

	glBindBuffer(GL_ARRAY_BUFFER, mesh.mb);
	glBufferData(GL_ARRAY_BUFFER, count * sizeof(Mat4), model_matrices, GL_DYNAMIC_DRAW);

	size_t vec_size = sizeof(Vec4);
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 4*vec_size, (void*)0);
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 4*vec_size, (void*)(1*vec_size));
	glEnableVertexAttribArray(5);
	glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4*vec_size, (void*)(2*vec_size));
	glEnableVertexAttribArray(6);
	glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4*vec_size, (void*)(3*vec_size));

	glVertexAttribDivisor(3, 1);
	glVertexAttribDivisor(4, 1);
	glVertexAttribDivisor(5, 1);
	glVertexAttribDivisor(6, 1);

	glBindBuffer(GL_ARRAY_BUFFER, mesh.nb);
	glBufferData(GL_ARRAY_BUFFER, count * sizeof(Mat3), normal_matrices, GL_DYNAMIC_DRAW);

	vec_size = sizeof(Vec3);
	glEnableVertexAttribArray(7);
	glVertexAttribPointer(7, 3, GL_FLOAT, GL_FALSE, 3*vec_size, (void*)0);
	glEnableVertexAttribArray(8);
	glVertexAttribPointer(8, 3, GL_FLOAT, GL_FALSE, 3*vec_size, (void*)(vec_size));
	glEnableVertexAttribArray(9);
	glVertexAttribPointer(9, 3, GL_FLOAT, GL_FALSE, 3*vec_size, (void*)(2*vec_size));

	glVertexAttribDivisor(7, 1);
	glVertexAttribDivisor(8, 1);
	glVertexAttribDivisor(9, 1);

	glDrawElementsInstanced(GL_TRIANGLES, mesh.e_count, GL_UNSIGNED_INT, 0, count);
	glBindVertexArray(0);
}

internal void
hdf_render_mesh_modulated(hdf_Mesh mesh, hdf_Transform *transform, Vec3 *colors, u32 count)
{
	glBindVertexArray(mesh.vao);
	glUseProgram(mesh.shader);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh.texture);
	glUniform1i(glGetUniformLocation (mesh.shader, "tex"), 0);

	glUniformMatrix4fv (glGetUniformLocation (mesh.shader, "projectionMatrix"),
			1, GL_FALSE, (f32*)(&hdf_Main_Camera.projection_Matrix));

	glUniformMatrix4fv (glGetUniformLocation (mesh.shader, "cameraMatrix"),
			1, GL_FALSE, (f32*)(&hdf_Main_Camera.cam_matrix));

	hdf_Transform *transf = transform;
	for(u32 i = 0; i < count; i++)
	{
		// Color modulation
		glUniform3fv(glGetUniformLocation(mesh.shader, "colorMod"), 1, (f32 *)(colors + i));

		Mat3 rot_m = mat3_from_quaternion(transf->rotation);
		Mat3 scale_m = (Mat3) {
			transf->scale.x, 0.0f, 0.0f,
			0.0f, transf->scale.y, 0.0f,
			0.0f, 0.0f, transf->scale.z
		};
		rot_m = mat3_mult(&rot_m, &scale_m);

		Mat4 model_m = (Mat4) {
			rot_m.x.x * transf->scale.x, rot_m.x.y, rot_m.x.z, transf->position.x,
			rot_m.y.x, rot_m.y.y * transf->scale.y, rot_m.y.z, transf->position.y,
			rot_m.z.x, rot_m.z.y, rot_m.z.z * transf->scale.z, transf->position.z,
			0.0f, 0.0f, 0.0f, 1.0f,
		};

		glUniformMatrix4fv (glGetUniformLocation (mesh.shader, "modelViewMatrix"),
				1, GL_FALSE, (f32*)(&model_m));

		// Normal matrix
		// The inverse and transpose is done on the shader
		f32 normal_matrix[9] = {
			model_m.e[0][0], model_m.e[0][1], model_m.e[0][2],
			model_m.e[1][0], model_m.e[1][1], model_m.e[1][2],
			model_m.e[2][0], model_m.e[2][1], model_m.e[2][2],
		};
		glUniformMatrix3fv (glGetUniformLocation (mesh.shader, "normalMatrix"),
				1, GL_FALSE, (f32*)(normal_matrix));

		glDrawElements(GL_TRIANGLES, mesh.e_count, GL_UNSIGNED_INT, 0);
		transf++;
	}
	glBindVertexArray(0);
}


internal GLuint
hdf_compile_shader(const char* source, GLenum type)
{
	GLuint shader = glCreateShader(type);
	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);

	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (!status)
	{
		char buffer[512];
		glGetShaderInfoLog(shader, 512, NULL, buffer);
		hdf_log_err("Shader %d failed to compile status %d %s", shader, status, buffer);
	}
	else
	{
		hdf_log("Shader %d compiled", shader);
	}

	return shader;
}

internal GLuint
hdf_create_shader_program(char* vertex_source, char* fragment_source)
{
	GLuint vertexShader = hdf_compile_shader(vertex_source, GL_VERTEX_SHADER);
	GLuint fragmentShader = hdf_compile_shader(fragment_source, GL_FRAGMENT_SHADER);

	// linking Shaders
	GLuint shaderProgram;
	shaderProgram = glCreateProgram();

	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);

	glBindFragDataLocation(shaderProgram, 0, "outColor");
	glLinkProgram(shaderProgram);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	return shaderProgram;
}

internal hdf_Sprite
hdf_create_sprite(GLuint shader, GLuint texture)
{
	hdf_Sprite sprite = {};
	sprite.x0 = 0.0f;
	sprite.y0 = 0.0f;
	sprite.x1 = 1.0f;
	sprite.y1 = 1.0f;

#if 0
	f32 vertices[] = {
		-0.5, -0.5, 0.0, sprite.x0, sprite.y1,
		 0.5, -0.5, 0.0, sprite.x1, sprite.y1,
		 0.5,  0.5, 0.0, sprite.x1, sprite.y0,
		-0.5,  0.5, 0.0, sprite.x0, sprite.y0,
	};
#else
	f32 vertices[] = {
		 1.0, 1.0, 0.0, 1.0f, 0.0f,
		-1.0, 1.0, 0.0, 0.0f, 1.0f,
		 1.0,-1.0, 0.0, 0.0f, 0.0f,
		-1.0,-1.0, 0.0, 1.0f, 1.0f,
	};
#endif
	u8 elements[] = {1,2,0,1,3,2};

	sprite.shader = shader;
	sprite.texture = texture;

	glGenVertexArrays(1, &sprite.vao);
	glBindVertexArray(sprite.vao);
	glEnableVertexAttribArray(sprite.vao);

	glGenBuffers (1, &sprite.vb);
	glBindBuffer(GL_ARRAY_BUFFER, sprite.vb);
	glBufferData(GL_ARRAY_BUFFER, 20*sizeof(f32), vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &sprite.eb);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sprite.eb);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6*sizeof(u32), elements, GL_STATIC_DRAW);

	glUseProgram(sprite.shader);

	// linking data and atributes
	GLint posAttrib = 0;
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 5*sizeof(f32), (void*)0);

	GLint uvCord = 1;
	glEnableVertexAttribArray(uvCord);
	glVertexAttribPointer(uvCord, 2, GL_FLOAT, GL_FALSE, 5*sizeof(f32), (void*)(3*sizeof(f32)));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	return sprite;
}

internal void
hdf_update_sprite(hdf_Sprite *sprite)
{
	f32 vertices[] = {
		-0.5, -0.5, 0.0, sprite->x0, sprite->y1,
		 0.5, -0.5, 0.0, sprite->x1, sprite->y1,
		 0.5,  0.5, 0.0, sprite->x1, sprite->y0,
		-0.5,  0.5, 0.0, sprite->x0, sprite->y0,
	};
	u8 elements[] = {0,1,3,1,2,3};

	glBindVertexArray(sprite->vao);

	//glBindBuffer(GL_ARRAY_BUFFER, sprite->vb);
	glBufferData(GL_ARRAY_BUFFER, 20*sizeof(f32), vertices, GL_DYNAMIC_DRAW);

	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sprite->eb);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6*sizeof(u32), elements, GL_DYNAMIC_DRAW);

	glUseProgram(sprite->shader);

	// linking data and atributes
	GLint posAttrib = 0;
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 5*sizeof(f32), (void*)0);

	GLint uvCord = 1;
	glEnableVertexAttribArray(uvCord);
	glVertexAttribPointer(uvCord, 2, GL_FLOAT, GL_FALSE, 5*sizeof(f32), (void*)(3*sizeof(f32)));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

internal hdf_Mesh
hdf_create_mesh(hdf_Vertices vertices[], u32 elements[],
			i32 vertice_size, i32 element_size,
			GLuint shader, GLuint texture)
{
	hdf_Mesh mesh;
	mesh.e_count = element_size / sizeof(u32);
	mesh.shader = shader;
	mesh.texture = texture;

	glGenVertexArrays(1, &mesh.vao);
	glBindVertexArray(mesh.vao);

	glGenBuffers (1, &mesh.vb);
	glBindBuffer(GL_ARRAY_BUFFER, mesh.vb);
	glBufferData(GL_ARRAY_BUFFER, vertice_size, vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &mesh.eb);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.eb);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, element_size ,elements, GL_STATIC_DRAW);

	glUseProgram(mesh.shader);

	// linking data and atributes
	GLint posAttrib = 0;
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 8*sizeof(f32), (void*)0);

	GLint normalAttr = 2;
	glEnableVertexAttribArray(normalAttr);
	glVertexAttribPointer(normalAttr, 3, GL_FLOAT, GL_FALSE, 8*sizeof(f32), (void*)(3*sizeof(f32)));

	GLint uvCord = 1;
	glEnableVertexAttribArray(uvCord);
	glVertexAttribPointer(uvCord, 2, GL_FLOAT, GL_FALSE, 8*sizeof(f32), (void*)(6*sizeof(f32)));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	return mesh;
}

internal hdf_Instance_Mesh
hdf_create_instance_mesh(hdf_Vertices vertices[], i32 elements[],
			i32 vertice_size, i32 element_size,
			GLuint shader, GLuint texture)
{
	hdf_Instance_Mesh mesh;
	mesh.e_count = element_size / sizeof(i32);
	mesh.shader = shader;
	mesh.texture = texture;

	glGenVertexArrays(1, &mesh.vao);
	glBindVertexArray(mesh.vao);

	glGenBuffers (1, &mesh.vb);
	glBindBuffer(GL_ARRAY_BUFFER, mesh.vb);
	glBufferData(GL_ARRAY_BUFFER, vertice_size, vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &mesh.eb);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.eb);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, element_size ,elements, GL_STATIC_DRAW);

	glGenBuffers(1, &mesh.mb);
	glGenBuffers(1, &mesh.nb);

	glUseProgram(mesh.shader);

	// linking data and atributes
	GLint posAttrib = 0;
	glEnableVertexAttribArray(posAttrib);
	glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 8*sizeof(f32), (void*)0);

	GLint normalAttr = 2;
	glEnableVertexAttribArray(normalAttr);
	glVertexAttribPointer(normalAttr, 3, GL_FLOAT, GL_FALSE, 8*sizeof(f32), (void*)(3*sizeof(f32)));

	GLint uvCord = 1;
	glEnableVertexAttribArray(uvCord);
	glVertexAttribPointer(uvCord, 2, GL_FLOAT, GL_FALSE, 8*sizeof(f32), (void*)(6*sizeof(f32)));

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	
	return mesh;
}

internal hdf_Instance_Mesh
hdf_create_instance_mesh(hdf_Mesh mesh)
{
	hdf_Instance_Mesh result;
	result.vao = mesh.vao;
	result.vb = mesh.vb;
	result.eb = mesh.eb;
	result.e_count = mesh.e_count;
	result.texture = mesh.texture;
	result.shader = mesh.shader;

	glBindVertexArray(result.vao);
	glGenBuffers(1, &result.mb);
	glGenBuffers(1, &result.nb);
	glBindVertexArray(0);

	return result;
}



internal inline
void hdf_destroy_mesh(hdf_Mesh mesh)
{
	glDeleteBuffers(1, &mesh.vb);
	glDeleteBuffers(1, &mesh.eb);
	glDeleteVertexArrays(1, &mesh.vao);
	glDeleteProgram(mesh.shader);
	glDeleteTextures(1, &mesh.texture);
}

internal inline
Mat4 hdf_create_ortogonal_matrix(f32 n, f32 f, f32 r, f32 t, f32 s)
{
	Mat4 result = {
		s/r , 0.0f, 0.0f, 0.0f,
		0.0f, s/t , 0.0f, 0.0f,
		0.0f, 0.0f, -(2*s)/(f-n), -(f+n)/(f-n),
		0.0f, 0.0f, 0.0f, 1.0f
	};
	return result;
}

internal inline
Mat4 hdf_create_projection_matrix(f32 n, f32 f, f32 r, f32 t)
{
	Mat4 result = {
		n/r , 0.0f, 0.0f, 0.0f,
		0.0f, n/t , 0.0f, 0.0f,
		0.0f, 0.0f, -(f+n)/(f-n),(-2*f*n)/(f-n),
		0.0f, 0.0f, -1.0f, 0.0f
	};
	return result;
}


internal hdf_Mesh
hdf_createMeshOBJ(const char* filename, GLuint shader)
{
	hdf_Mesh mesh = {0};

	FILE* fn = fopen(filename, "r");
	if(fn != NULL)
	{
		fseek(fn, 0, SEEK_END);
		long size = ftell(fn) + 1;
		fseek(fn, 0, SEEK_SET);

		Vec3 *vb = (Vec3*)calloc(size/4, sizeof(Vec3));
		Vec2 *tb = (Vec2*)calloc(size/4, sizeof(Vec3));
		Vec3 *nb = (Vec3*)calloc(size/4, sizeof(Vec3));

		i32 *fvb = (i32*)calloc(size/4,sizeof(i32));
		i32 *ftb = (i32*)calloc(size/4,sizeof(i32));
		i32 *fnb = (i32*)calloc(size/4,sizeof(i32));

		i32 vbc = 0, tbc = 0, nbc = 0, fbc = 0;

		while(!feof(fn))
		{
			char c = fgetc(fn);
			char buffer[256];
			if(c == '#' || c == 'o' || c == 's' || c == 'u' || c == 'm')
			{
				do {
					c = fgetc(fn);
				} while(c != '\n');
			}

			if(c == 'v')
			{
				c = fgetc(fn);
				if(c == ' ')
				{
					int i = 0;
					do{
						c = fgetc(fn);
						hd_assert(i < 256);
						buffer[i++] = c;
					}while(c != '\n');
					buffer[i-1] = '\0';
					hd_assert(vbc < size/4);
					sscanf(buffer, "%f %f %f", &(vb[vbc].x), &(vb[vbc].y), &(vb[vbc].z));
					vbc++;
				}

				if(c == 't')
				{
					int i = 0;
					do{
						hd_assert(i < 256);
						c = fgetc(fn);
						buffer[i++] = c;
					}while(c != '\n');
					buffer[i-1] = '\0';
					hd_assert(tbc < size/4);
					sscanf(buffer, "%f %f", &(tb[tbc].x), &(tb[tbc].y));
					tbc++;
				}

				if(c == 'n')
				{
					int i = 0;
					do{
						hd_assert(i < 256);
						c = fgetc(fn);
						buffer[i++] = c;
					}while(c != '\n');
					buffer[i-1] = '\0';
					hd_assert(nbc < size/4);
					//printf("%s\n", buffer);
					sscanf(buffer, "%f %f %f", &(nb[nbc].x), &(nb[nbc].y), &(nb[nbc].z));
					nbc++;
				}
			}

			if(c == 'f')
			{
				int i = 0;
				do{
					hd_assert(i < 256);
					c = fgetc(fn);
					if(c == '/')
						buffer[i++] = ' ';
					else
						buffer[i++] = c;
				} while(c != '\n');
				buffer[i] = '\0';
				hd_assert(fbc < size/4 - 4);
				sscanf(buffer, "%i %i %i %i %i %i %i %i %i",
					&(fvb[fbc])  , &(ftb[fbc])  , &(fnb[fbc]),
					&(fvb[fbc+1]), &(ftb[fbc+1]), &(fnb[fbc+1]),
					&(fvb[fbc+2]), &(ftb[fbc+2]), &(fnb[fbc+2]));
				fbc += 3;
			}
		}

		int maior = 0;
		for(int i = 0; i < fbc; i++)
		{
			if(ftb[i] > maior)
				maior = ftb[i];
		}
		//fbc-=2;

		hdf_Vertices *vertices = (hdf_Vertices*)calloc(maior, sizeof(hdf_Vertices));
		u32 *elements = (u32*)calloc(fbc, sizeof(u32));

		for(int i = 0; i < fbc; i++) {
			int j = fvb[i] - 1;
			int k = ftb[i] - 1;
			int l = fnb[i] - 1;

			vertices[k].position = vb[j];
			vertices[k].uv_cordinates = tb[k];
			vertices[k].normal = nb[l];

			elements[i] = k;
		}

		mesh = hdf_create_mesh(vertices, elements, maior*sizeof(hdf_Vertices),
				fbc*sizeof(i32), shader, 0);

		free(elements);
		free(vertices);
		free(vb);
		free(tb);
		free(nb);
		free(fnb);
		free(ftb);
		free(fvb);
	}
	fclose(fn);
	return mesh;
}

#endif

