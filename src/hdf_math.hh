#ifndef HDF_MATH_H
#define HDF_MATH_H
//#define SIMD

#include <math.h>
#include <emmintrin.h>
#include "hdf_types.h"

#define internal static

// TODO: Maybe change this typedefs to another place
// typedefs
struct Vec2 {
	f32 x;
	f32 y;
};

struct Vec2i {
	i32 x;
	i32 y;
};

union Vec3 {
	struct {
		f32 x;
		f32 y;
		f32 z;
	};
	struct {
		f32 r;
		f32 g;
		f32 b;
	};
	f32 vec[3];
};

struct Vec3i {
	i32 x;
	i32 y;
	i32 z;
};

union Vec4 {
	struct {
		Vec3 xyz;
		f32 w_;
	};
	struct {
		f32 x, y, z, w;
	};
	struct {
		f32 r, g, b, a;
	};
	f32 e[4];
};

union Mat4 {
	struct {
		Vec4 x;
		Vec4 y;
		Vec4 z;
		Vec4 w;
	};
	f32 e[4][4];
	f32 el[16];
};

union Mat3 {
	struct {
		Vec3 x;
		Vec3 y;
		Vec3 z;
	};
	f32 e[3][3];
	f32 el[9];
};

internal inline Vec2
vec2(f32 x, f32 y) { return (Vec2){x, y};}

internal inline Vec3
vec3(f32 x, f32 y, f32 z) { return (Vec3){{x, y, z}};}

internal inline Vec3i
vec3i(i32 x, i32 y, i32 z) { return (Vec3i){x, y, z};}

// ==== Vec2 Operators ========================================================
internal inline Vec2
operator+(Vec2 v1, Vec2 v2) { return (Vec2){v1.x + v2.x, v1.y + v2.y};}

internal inline Vec2
operator-(Vec2 v1, Vec2 v2) { return (Vec2){v1.x - v2.x, v1.y - v2.y};}

internal inline Vec2
operator*(Vec2 v, f32 f) { return (Vec2){v.x * f, v.y * f};}

internal inline Vec2
operator*(f32 f, Vec2 v) { return (Vec2){v.x * f, v.y * f};}

internal inline Vec2
operator/(Vec2 v, f32 f) { return (Vec2){v.x / f, v.y / f};}

internal inline Vec2
operator/(f32 f, Vec2 v) { return (Vec2){v.x / f, v.y / f};}

internal inline void
operator+=(Vec2& v1, Vec2 v2) { v1 = v1 + v2;}

internal inline void
operator-=(Vec2 &v1, Vec2 v2) { v1 = v1 - v2;}

internal inline void
operator*=(Vec2 &v, f32 f) { v = v*f;}

internal inline void
operator/=(Vec2 &v, f32 f) { v = v*f;}

internal inline bool
operator==(Vec2 v1, Vec2 v2) { return (v1.x == v2.x && v1.y == v2.y);};

internal inline bool
operator!=(Vec2 v1, Vec2 v2) { return (v1.x != v2.x || v1.y != v2.y);};

internal inline f32
length_squared(Vec2 vec)
{
	return vec.x*vec.x + vec.y*vec.y;
}

internal inline f32
length(Vec2 vec)
{
	return sqrtf(vec.x*vec.x + vec.y*vec.y);
}

internal inline Vec2
normalized(Vec2 vec)
{
	f32 l = length(vec);
	if (l > 0)
		return vec / length(vec);
	else
		return (Vec2){0.0f, 0.0f};
}

internal inline Vec2
v2_lerp(Vec2 va, Vec2 vb, f32 t)
{
	Vec2 result;
	result.x = t * (vb.x - va.x) + va.x;
	result.y = t * (vb.y - va.y) + va.y;
	return result;
}


// ==== Vec2i Operators =======================================================
internal inline Vec2i
operator+(Vec2i v1, Vec2i v2) { return (Vec2i){v1.x + v2.x, v1.y + v2.y};}

internal inline Vec2i
operator-(Vec2i v1, Vec2i v2) { return (Vec2i){v1.x - v2.x, v1.y - v2.y};}

internal inline Vec2i
operator*(Vec2i v, int i) { return (Vec2i){v.x * i, v.y * i};}

internal inline Vec2i
operator*(int i, Vec2i v) { return (Vec2i){v.x * i, v.y * i};}

internal inline Vec2i
operator/(Vec2i v, int i) { return (Vec2i){v.x / i, v.y / i};}

internal inline Vec2i
operator/(int i, Vec2i v) { return (Vec2i){v.x / i, v.y / i};}

internal inline void
operator+=(Vec2i& v1, Vec2i v2) { v1 = v1 + v2;}

internal inline void
operator-=(Vec2i &v1, Vec2i v2) { v1 = v1 - v2;}

internal inline void
operator*=(Vec2i &v, f32 f) { v = v*f;}

internal inline void
operator/=(Vec2i &v, f32 f) { v = v*f;}

internal inline bool
operator==(Vec2i v1, Vec2i v2) { return (v1.x == v2.x && v1.y == v2.y);};

internal inline bool
operator!=(Vec2i v1, Vec2i v2) { return (v1.x != v2.x || v1.y != v2.y);};

internal inline Vec2i
v2_to_v2i(Vec2 vec)
{
	return (Vec2i){(i32)(vec.x),(i32)(vec.y)};
}

internal inline Vec2i
v2_to_rounded_v2i(Vec2 vec)
{
	return (Vec2i){(i32)(vec.x + 0.5),(i32)(vec.y + 0.5)};
}

// ==== Vec3 Operators ========================================================
internal inline Vec3
operator+(Vec3 v1, Vec3 v2)
{ 
	Vec3 result = (Vec3){{v1.x + v2.x, v1.y + v2.y, v1.z + v2.z}};
	return result;
}

internal inline Vec3
operator-(Vec3 v1, Vec3 v2) {
	Vec3 result = (Vec3){{v1.x - v2.x, v1.y - v2.y, v1.z - v2.z}};
	return result;
}

internal inline Vec3
operator*(Vec3 v, f32 f) {
	Vec3 result = (Vec3){{v.x * f, v.y * f, v.z * f}};
	return result;
}

internal inline Vec3
operator*(f32 f, Vec3 v) {
	Vec3 result = (Vec3){{v.x * f, v.y * f, v.z * f}};
	return result;
}

internal inline Vec3
operator/(Vec3 v, f32 f) {
	Vec3 result = (Vec3){{v.x / f, v.y / f, v.z / f}};
	return result;
}

internal inline Vec3
operator/(f32 f, Vec3 v) {
	Vec3 result = (Vec3){{v.x / f, v.y / f, v.z / f}};
	return result;
}

internal inline void
operator+=(Vec3& v1, Vec3 v2) { v1 = v1 + v2;}

internal inline void
operator-=(Vec3 &v1, Vec3 v2) { v1 = v1 - v2;}

internal inline void
operator*=(Vec3 &v, float f) { v = v*f;}

internal inline void
operator/=(Vec3 &v, f32 f) { v = v*f;}

internal inline Vec3
operator-(Vec3 v) { return (Vec3){-v.x, -v.y, -v.z};}

internal inline bool
operator==(Vec3 v1, Vec3 v2) { return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);};

internal inline bool
operator!=(Vec3 v1, Vec3 v2) { return (v1.x != v2.x || v1.y != v2.y || v1.z != v2.z);};

internal inline Vec3i
v3_to_rounded_v3i(Vec3 vec)
{
	return (Vec3i){(i32)(vec.x + 0.5f),(i32)(vec.y + 0.5f),(i32)(vec.z + 0.5f)};
}

internal inline f32
length_squared(Vec3 v)
{
	f32 result = v.x*v.x + v.y*v.y + v.z*v.z;
	return result;
}

internal inline f32
length(Vec3 v)
{
	f32 result = sqrtf(v.x*v.x + v.y*v.y + v.z*v.z);
	return result;
}

internal inline Vec3
normalized(Vec3 vec)
{
	f32 l = length(vec);
	if (l > 0)
		return vec / length(vec);
	else
		return (Vec3){0.0f, 0.0f, 0.0f};
}

internal inline Vec3
simd_v3_lerp(f32* va, f32* vb, f32 t)
{
	__m128 sva = _mm_load_ps(va);
	__m128 svb = _mm_load_ps(vb);
	__m128 st = _mm_set_ps1(t);
	__m128 result = _mm_add_ps(_mm_mul_ps(st, _mm_sub_ps(svb, sva)), sva);
	return *((Vec3*)(&result));
}

// TODO: Check if the SIMD version is more efficient
internal inline Vec3
lerp(Vec3 va, Vec3 vb, float t)
{
	Vec3 result;
#ifdef SIMD
	result = simd_v3_lerp((f32*)(&va), (f32*)(&vb), t);
#else
	result.x = t * (vb.x - va.x) + va.x;
	result.y = t * (vb.y - va.y) + va.y;
	result.z = t * (vb.z - va.z) + va.z;
#endif
	return result;
}

internal inline float
dot(Vec3 v1, Vec3 v2)
{
	f32 result = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
	return result;
}

internal inline Vec3
hadamard(Vec3 v1, Vec3 v2)
{
	Vec3 result = (Vec3){{v1.x*v2.x, v1.y*v2.y, v1.z*v2.z}};
	return result;
}

internal inline Vec3
cross(Vec3 v1, Vec3 v2)
{
	Vec3 result;
	result.x = v1.y*v2.z - v1.z*v2.y;
	result.y = v1.z*v2.x - v1.x*v2.z;
	result.z = v1.x*v2.y - v1.y*v2.x;
	return result;
}

// ==== Vec3i Operators =======================================================
internal inline Vec3i
operator+(Vec3i v1, Vec3i v2) { return (Vec3i){v1.x + v2.x, v1.y + v2.y, v1.z + v2.z};}

internal inline Vec3i
operator-(Vec3i v1, Vec3i v2) { return (Vec3i){v1.x - v2.x, v1.y - v2.y, v1.z - v2.z};}

internal inline Vec3i
operator-(Vec3i v) { return (Vec3i){-v.x, -v.y, -v.z};}

internal inline Vec3i
operator*(Vec3i v, int i) { return (Vec3i){v.x * i, v.y * i, v.z * i};}

internal inline Vec3i
operator*(int i, Vec3i v) { return (Vec3i){v.x * i, v.y * i, v.z * i};}

internal inline Vec3i
operator/(Vec3i v, int i) { return (Vec3i){v.x / i, v.y / i, v.z / i};}

internal inline Vec3i
operator/(int i, Vec3i v) { return (Vec3i){v.x / i, v.y / i, v.z / i};}

internal inline void
operator+=(Vec3i& v1, Vec3i v2) { v1 = v1 + v2;}
internal inline void
operator-=(Vec3i &v1, Vec3i v2) { v1 = v1 - v2;}

internal inline void
operator*=(Vec3i &v, f32 f) { v = v*f;}
internal inline void
operator/=(Vec3i &v, f32 f) { v = v*f;}

internal inline bool
operator==(Vec3i v1, Vec3i v2) { return (v1.x == v2.x && v1.y == v2.y && v1.y == v2.y);};
internal inline bool
operator!=(Vec3i v1, Vec3i v2) { return (v1.x != v2.x || v1.y != v2.y || v1.z != v2.z);};

internal inline Vec3i
v3_to_v3i(Vec3 vec)
{
	return (Vec3i){(i32)(vec.x),(i32)(vec.y),(i32)(vec.z)};
}

internal inline Vec2
v3_to_v2(Vec3 v){ return (Vec2){v.x, v.y}; };
internal inline Vec3
v2_to_v3(Vec2 v){ return (Vec3){v.x, v.y, 0.0f}; };

internal inline Vec2i
v3i_to_v2i(Vec3i v){ return (Vec2i){v.x, v.y}; };
internal inline Vec3i
v2i_to_v3i(Vec2i v){ return (Vec3i){v.x, v.y, 0}; };


// ===== Vec4 =================================================================

internal inline Vec4
vec4(f32 x, f32 y, f32 z, f32 w)
{
	return (Vec4){{x, y, z, w}};
}

internal inline Vec4
vec4(Vec3 v, f32 w)
{
	return (Vec4){{v, w}};
}

internal inline Vec4
operator+(Vec4 a, Vec4 b) {
	Vec4 result = {a.x + b.x, a.y + b.y, a.z + b.z,  a.w + b.w};
	return result;
}

internal inline Vec4
operator*(Vec4 q, f32 f) {
	return (Vec4){q.x * f, q.y * f, q.z * f, q.w * f};
}

internal inline Vec4
operator/(Vec4 q, f32 f) {
	return (Vec4){q.x / f, q.y / f, q.z / f, q.w / f};
}

internal inline Vec4
quat_mul(Vec4 a, Vec4 b) {
	Vec4 result = {cross(a.xyz,b.xyz) + b.xyz*a.w + a.xyz*b.w, a.w*b.w - dot(a.xyz,b.xyz)};
	return result;
}

internal inline Vec4
quat_conj(Vec4 q) {
	return (Vec4){q.xyz * -1, q.w};
}

internal inline f32
length_squared(Vec4 q) {
	return q.x*q.x + q.y*q.y + q.z*q.z + q.w*q.w;
}

internal inline f32
length(Vec4 q) {
	return sqrtf(length_squared(q));
}

internal inline Vec4
normalized(Vec4 q) {
	return q / length(q);
}

internal inline Vec4
euler_to_quat(Vec3 e) {
	Vec4 q;

	f32 cy = cosf(e.z * 0.5f);
	f32 sy = sinf(e.z * 0.5f);
	f32 cp = cosf(e.y * 0.5f);
	f32 sp = sinf(e.y * 0.5f);
	f32 cr = cosf(e.x * 0.5f);
	f32 sr = sinf(e.x * 0.5f);

	f32 cp_sr = cp*sr;
	f32 cp_cr = cp*cr;

	f32 sp_sr = sp*sr;
	f32 sp_cr = sp*cr;

	q.x = cy * cp_sr - sy * sp_cr;
	q.y = sy * cp_sr + cy * sp_cr;
	q.z = sy * cp_cr - cy * sp_sr;
	q.w = cy * cp_cr + sy * sp_sr;

	return q;
}

Vec3 rotation_quat(Vec3 v, Vec4 r) {
	Vec4 result = quat_mul(quat_mul(r, (Vec4){v, 0.0f}), quat_conj(r));
	return result.xyz;
}

internal inline Vec3
quat_to_euler(Vec4 q) {
	f32 qys = q.y*q.y * 2;
	f32 qxs = q.x*q.x * 2;
	f32 qzs = q.z*q.z * 2;
	f32 qwd = q.w*2;

	return (Vec3){
		atan2f(qwd * q.x + 2 * q.y * q.z, 1 - qxs - qys),
		asinf(qwd * q.y - 2 * q.x * q.z),
		atan2f(qwd * q.z + 2 * q.y * q.x, 1 - qzs - qys)
	};
}

// ==== Mat4 ==================================================================


internal inline void
simd_mat4_mul(f32 *m1, f32 *m2, f32 *m_out)
{
	__m128 m2_0 = _mm_load_ps(m2 + 0);
	__m128 m2_1 = _mm_load_ps(m2 + 4);
	__m128 m2_2 = _mm_load_ps(m2 + 8);
	__m128 m2_3 = _mm_load_ps(m2 + 12);

	for (int i = 0; i < 4; i++)
	{
		__m128 m1_i0 = _mm_set_ps1(m1[i*4 + 0]);
		__m128 m1_i1 = _mm_set_ps1(m1[i*4 + 1]);
		__m128 m1_i2 = _mm_set_ps1(m1[i*4 + 2]);
		__m128 m1_i3 = _mm_set_ps1(m1[i*4 + 3]);

		__m128 mul1 = _mm_mul_ps(m2_0, m1_i0);
		__m128 mul2 = _mm_mul_ps(m2_1, m1_i1);
		__m128 mul3 = _mm_mul_ps(m2_2, m1_i2);
		__m128 mul4 = _mm_mul_ps(m2_3, m1_i3);

		__m128 line = _mm_add_ps(mul1, mul2);
		line = _mm_add_ps(line, mul3);
		line = _mm_add_ps(line, mul4);

		_mm_store_ps((m_out + i * 4), line);
	}
}

internal inline Mat4
mat4_mult(Mat4* m1, Mat4* m2)
{
	Mat4 tmp = {};
#ifdef SIMD
	simd_mat4_mul((f32*)m1, (f32*)m2, (f32*)(&tmp));
#else
	for (int i = 0; i < 4; i++)
	{
		tmp.e[i][0] = m1->e[i][0] * m2->e[0][0] + m1->e[i][1] * m2->e[1][0] + m1->e[i][2] * m2->e[2][0] + m1->e[i][3] * m2->e[3][0];
		tmp.e[i][1] = m1->e[i][0] * m2->e[0][1] + m1->e[i][1] * m2->e[1][1] + m1->e[i][2] * m2->e[2][1] + m1->e[i][3] * m2->e[3][1];
		tmp.e[i][2] = m1->e[i][0] * m2->e[0][2] + m1->e[i][1] * m2->e[1][2] + m1->e[i][2] * m2->e[2][2] + m1->e[i][3] * m2->e[3][2];
		tmp.e[i][3] = m1->e[i][0] * m2->e[0][3] + m1->e[i][1] * m2->e[1][3] + m1->e[i][2] * m2->e[2][3] + m1->e[i][3] * m2->e[3][3];
	}
#endif
	return tmp;
}

internal inline Mat3
mat3_mult(Mat3* m1, Mat3* m2)
{
	Mat3 tmp = {};
	for (int i = 0; i < 3; i++)
	{
		tmp.e[i][0] = m1->e[i][0] * m2->e[0][0] + m1->e[i][1] * m2->e[1][0] + m1->e[i][2] * m2->e[2][0];
		tmp.e[i][1] = m1->e[i][0] * m2->e[0][1] + m1->e[i][1] * m2->e[1][1] + m1->e[i][2] * m2->e[2][1];
		tmp.e[i][2] = m1->e[i][0] * m2->e[0][2] + m1->e[i][1] * m2->e[1][2] + m1->e[i][2] * m2->e[2][2];
	}
	return tmp;
}

internal inline void
simd_mat4_from_quaternion(f32* q, f32 *m)
{
#define M(x, i) ((f32*)(&(x)))[(i)]
	__m128 sq  = _mm_load_ps(q);
	__m128 ssq = _mm_mul_ps(sq, sq);

	__m128 tmpw = _mm_set_ps1(M(ssq, 0));

	__m128 tmpx = _mm_mul_ps(_mm_set_ps(1.0f, -1.0f, -1.0f, 1.0f), _mm_set_ps1(M(ssq, 1)));
	__m128 tmpy = _mm_mul_ps(_mm_set_ps(1.0f, -1.0f, 1.0f, -1.0f), _mm_set_ps1(M(ssq, 2)));
	__m128 tmpz = _mm_mul_ps(_mm_set_ps(1.0f, 1.0f, -1.0f, -1.0f), _mm_set_ps1(M(ssq, 3)));

	__m128 tmp = _mm_add_ps(tmpx, _mm_add_ps(tmpy, _mm_add_ps(tmpz, tmpw)));

    *(m + 0) =  M(tmp, 0);
    *(m + 5) =  M(tmp, 1);
    *(m + 10) = M(tmp, 2);

	tmp  = _mm_mul_ps(_mm_set_ps(q[1], q[2], q[1], q[3]), _mm_set_ps(q[3], q[0], q[2], q[0]));

	__m128 tmp2 = _mm_mul_ps(_mm_set_ps(-1.0f, 1.0f, -1.0f, 1.0f), _mm_set_ps(M(tmp, 2), M(tmp, 2), M(tmp, 0), M(tmp, 0)));
	tmp2 = _mm_add_ps(_mm_set_ps(M(tmp, 3), M(tmp, 3), M(tmp, 1), M(tmp, 1)), tmp2);
	tmp2 = _mm_mul_ps(_mm_set_ps(2.0f, 2.0f, 2.0f, 2.0f), tmp2);

    *(m + 8) = M(tmp2, 3);
    *(m + 2) = M(tmp2, 2);
	*(m + 1) = M(tmp2, 1);
    *(m + 4) = M(tmp2, 0);

	tmp  = _mm_mul_ps(_mm_set_ps(q[2], q[1], 1.0f, 1.0f), _mm_set_ps(q[3], q[0], 1.0f, 1.0f));
	tmp2 = _mm_mul_ps(_mm_set_ps(-1.0f, 1.0f, 1.0f, 1.0f), _mm_set_ps(M(tmp, 2), M(tmp, 2), 1.0f, 1.0f));
	tmp2 = _mm_add_ps(_mm_set_ps(M(tmp, 3), M(tmp, 3), 0.0f, 0.0f), tmp2);
	tmp2 = _mm_mul_ps(_mm_set_ps(2.0f, 2.0f, 1.0f, 1.0f), tmp2);

    *(m + 6) = M(tmp2, 3);
    *(m + 9) = M(tmp2, 2);

	*(m + 15) = 1.0;
#undef M
}

Mat4 mat4_from_quaternion(Vec4 q)
{
	Mat4 m = {0};
#ifdef SIMD
	simd_mat4_from_quaternion((f32*)(&q), (f32*)(&m));
#else
	f32 sqw = q.w * q.w;
    f32 sqx = q.x * q.x;
    f32 sqy = q.y * q.y;
    f32 sqz = q.z * q.z;

    f32 xy = q.x * q.y;
    f32 zw = q.z * q.w;

	f32 xz = q.x * q.z;
	f32 yw = q.y * q.w;

	f32 yz = q.y * q.z;
	f32 xw = q.x * q.w;

    m.x.x = ( sqx - sqy - sqz + sqw);
    m.y.y = (-sqx + sqy - sqz + sqw);
    m.z.z = (-sqx - sqy + sqz + sqw);

    m.y.x = 2.0 * (xy + zw);
    m.x.y = 2.0 * (xy - zw);

    m.z.x = 2.0 * (xz - yw);
    m.x.z = 2.0 * (xz + yw);

    m.z.y = 2.0 * (yz + xw);
    m.y.z = 2.0 * (yz - xw);

	m.w.w = 1.0;
#endif
	return m;
}

Mat3 mat3_from_quaternion(Vec4 q)
{
	Mat3 m = {0};
	f32 sqw = q.w * q.w;
    f32 sqx = q.x * q.x;
    f32 sqy = q.y * q.y;
    f32 sqz = q.z * q.z;

    f32 xy = q.x * q.y;
    f32 zw = q.z * q.w;

	f32 xz = q.x * q.z;
	f32 yw = q.y * q.w;

	f32 yz = q.y * q.z;
	f32 xw = q.x * q.w;

    m.x.x = ( sqx - sqy - sqz + sqw);
    m.y.y = (-sqx + sqy - sqz + sqw);
    m.z.z = (-sqx - sqy + sqz + sqw);

    m.y.x = 2.0 * (xy + zw);
    m.x.y = 2.0 * (xy - zw);

    m.z.x = 2.0 * (xz - yw);
    m.x.z = 2.0 * (xz + yw);

    m.z.y = 2.0 * (yz + xw);
    m.y.z = 2.0 * (yz - xw);
	return m;
}

#ifdef SIMD
	#undef SIMD
#endif

#endif
