// DO NOT include this file

// Change the keymap and the macros based on your need

#define INPUT_LEN 6 // The size of the Key_Map, max of 32

// Keymap for the game
int Key_Map[INPUT_LEN] = {
    GLFW_KEY_W,
    GLFW_KEY_A,
    GLFW_KEY_S,
    GLFW_KEY_D,
    GLFW_KEY_SPACE,
	GLFW_KEY_LEFT_SHIFT,
};

// Macros for the key values
#define KEY_W 0x01
#define KEY_A 0x02
#define KEY_S 0x04
#define KEY_D 0x08

#define KEY_Space 0x010
#define KEY_Shift 0x020
//#define KEY_ 0x040
//#define KEY_ 0x080

//#define KEY_ 0x100
//#define KEY_ 0x200
//#define KEY_ 0x400
//#define KEY_ 0x800

//#define KEY_ 0x1000
//#define KEY_ 0x2000
//#define KEY_ 0x4000
//#define KEY_ 0x8000
