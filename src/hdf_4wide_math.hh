#ifndef HDF_4WIDE_MATH
#define HDF_4WIDE_MATH

#include <emmintrin.h>
#include "hdf_types.h"

union f32_4x
{
	__m128 mm;
	f32 e[4];
};

union Vec3_4x
{
	struct
	{
		f32_4x x;
		f32_4x y;
		f32_4x z;
	};

	struct
	{
		f32_4x r;
		f32_4x g;
		f32_4x b;
	};

	struct
	{
		f32 ex[4];
		f32 ey[4];
		f32 ez[4];
	};
};


//
// ==== f32_4x ================================================================
//

internal inline f32_4x
operator+(f32_4x a, f32_4x b)
{
	f32_4x result;
	result.mm = _mm_add_ps(a.mm, b.mm);
	return result;
}

internal inline f32_4x
operator-(f32_4x a, f32_4x b)
{
	f32_4x result;
	result.mm = _mm_sub_ps(a.mm, b.mm);
	return result;
}

internal inline f32_4x
operator*(f32_4x a, f32_4x b)
{
	f32_4x result;
	result.mm = _mm_mul_ps(a.mm, b.mm);
	return result;
}

internal inline f32_4x
operator/(f32_4x a, f32_4x b)
{
	f32_4x result;
	result.mm = _mm_div_ps(a.mm, b.mm);
	return result;
}

internal inline void
operator+=(f32_4x &a, f32_4x b)
{
	a.mm = _mm_add_ps(a.mm, b.mm);
}

internal inline void
operator-=(f32_4x &a, f32_4x b)
{
	a.mm = _mm_sub_ps(a.mm, b.mm);
}

internal inline void
operator*=(f32_4x &a, f32_4x b)
{
	a.mm = _mm_mul_ps(a.mm, b.mm);
}

internal inline void
operator/=(f32_4x &a, f32_4x b)
{
	a.mm = _mm_div_ps(a.mm, b.mm);
}

internal inline f32_4x
square_root(f32_4x f)
{
	f32_4x result;
	result.mm = _mm_sqrt_ps(f.mm);
	return result;
}

internal inline f32_4x
square(f32_4x f)
{
	f32_4x result;
	result.mm = _mm_mul_ps(f.mm, f.mm);
	return result;
}

internal inline f32_4x
F32_4x(f32 *f)
{
	f32_4x result;
	result.mm = _mm_load_ps(f);
	return result;
}

internal inline f32_4x
F32_4x(f32 f)
{
	f32_4x result;
	result.mm = _mm_set1_ps(f);
	return result;
}

internal inline f32_4x
F32_4x(f32 a, f32 b, f32 c, f32 d)
{
	f32_4x result;
	result.mm = _mm_set_ps(a, b, c, d);
	return result;
}

//
// ==== Vec3_4x ===============================================================
//
internal inline Vec3_4x
operator+(Vec3_4x a, Vec3_4x b)
{
	Vec3_4x result;
	result.x.mm = _mm_add_ps(a.x.mm, b.x.mm);
	result.y.mm = _mm_add_ps(a.y.mm, b.y.mm);
	result.z.mm = _mm_add_ps(a.z.mm, b.z.mm);
	return result;
}

internal inline Vec3_4x
operator-(Vec3_4x a, Vec3_4x b)
{
	Vec3_4x result;
	result.x.mm = _mm_sub_ps(a.x.mm, b.x.mm);
	result.y.mm = _mm_sub_ps(a.y.mm, b.y.mm);
	result.z.mm = _mm_sub_ps(a.z.mm, b.z.mm);
	return result;
}

internal inline Vec3_4x
operator*(Vec3_4x v, f32_4x f)
{
	Vec3_4x result;
	result.x.mm = _mm_mul_ps(v.x.mm, f.mm);
	result.y.mm = _mm_mul_ps(v.y.mm, f.mm);
	result.z.mm = _mm_mul_ps(v.z.mm, f.mm);
	return result;
}

internal inline Vec3_4x
operator/(Vec3_4x v, f32_4x f)
{
	Vec3_4x result;
	result.x.mm = _mm_div_ps(v.x.mm, f.mm);
	result.y.mm = _mm_div_ps(v.y.mm, f.mm);
	result.z.mm = _mm_div_ps(v.z.mm, f.mm);
	return result;
}

internal inline Vec3_4x
operator*(f32_4x f, Vec3_4x v)
{
	Vec3_4x result;
	result.x.mm = _mm_mul_ps(v.x.mm, f.mm);
	result.y.mm = _mm_mul_ps(v.y.mm, f.mm);
	result.z.mm = _mm_mul_ps(v.z.mm, f.mm);
	return result;
}

internal inline Vec3_4x
operator/(f32_4x f, Vec3_4x v)
{
	Vec3_4x result;
	result.x.mm = _mm_div_ps(v.x.mm, f.mm);
	result.y.mm = _mm_div_ps(v.y.mm, f.mm);
	result.z.mm = _mm_div_ps(v.z.mm, f.mm);
	return result;
}


internal inline void
operator+=(Vec3_4x &a, Vec3_4x b)
{
	a.x.mm = _mm_add_ps(a.x.mm, b.x.mm);
	a.y.mm = _mm_add_ps(a.y.mm, b.y.mm);
	a.z.mm = _mm_add_ps(a.z.mm, b.z.mm);
}

internal inline void
operator-=(Vec3_4x &a, Vec3_4x b)
{
	a.x.mm = _mm_sub_ps(a.x.mm, b.x.mm);
	a.y.mm = _mm_sub_ps(a.y.mm, b.y.mm);
	a.z.mm = _mm_sub_ps(a.z.mm, b.z.mm);
}

internal inline void
operator*=(Vec3_4x &v, f32_4x f)
{
	v.x.mm = _mm_mul_ps(v.x.mm, f.mm);
	v.y.mm = _mm_mul_ps(v.y.mm, f.mm);
	v.z.mm = _mm_mul_ps(v.z.mm, f.mm);
}

internal inline void
operator/=(Vec3_4x &v, f32_4x f)
{
	v.x.mm = _mm_div_ps(v.x.mm, f.mm);
	v.y.mm = _mm_div_ps(v.y.mm, f.mm);
	v.z.mm = _mm_div_ps(v.z.mm, f.mm);
}

internal inline Vec3_4x
hadmard(Vec3_4x a, Vec3_4x b)
{
	Vec3_4x result;
	result.x.mm = _mm_mul_ps(a.x.mm, b.x.mm);
	result.y.mm = _mm_mul_ps(a.y.mm, b.y.mm);
	result.z.mm = _mm_mul_ps(a.z.mm, b.z.mm);
	return result;
}

//TODO: Check if this is the proper name
internal inline Vec3_4x
hadmard_div(Vec3_4x a, Vec3_4x b)
{
	Vec3_4x result;
	result.x.mm = _mm_div_ps(a.x.mm, b.x.mm);
	result.y.mm = _mm_div_ps(a.y.mm, b.y.mm);
	result.z.mm = _mm_div_ps(a.z.mm, b.z.mm);
	return result;
}

internal inline f32_4x
dot(Vec3_4x a, Vec3_4x b)
{
	f32_4x result = a.x*b.x + a.y*b.y + a.z*b.z;
	return result;	
}

internal inline f32_4x
length_squared(Vec3_4x v)
{
	f32_4x result = dot(v, v);
	return result;
}

internal inline f32_4x
length(Vec3_4x v)
{
	f32_4x result = square_root(dot(v, v));
	return result;
}

internal inline Vec3_4x
cross(Vec3_4x a, Vec3_4x b)
{
	Vec3_4x result;
	result.x = a.y*b.z - a.z*b.y;
	result.y = a.z*b.x - a.x*b.z;
	result.z = a.x*b.y - a.y*b.x;
	return result;
}

internal inline Vec3_4x
vec3_4x(f32_4x x, f32_4x y, f32_4x z)
{
	Vec3_4x result;
	result.x = x;
	result.y = y;
	result.z = z;
	return result;
}

internal inline Vec3_4x
vec3_4x(f32 *x, f32 *y, f32 *z)
{
	Vec3_4x result;
	result.x.mm = _mm_load_ps(x);
	result.y.mm = _mm_load_ps(y);
	result.z.mm = _mm_load_ps(z);
	return result;
}

#endif
