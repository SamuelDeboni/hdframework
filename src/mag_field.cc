#include "hdframework.hh"
#include "hdf_math.hh"
#include "hdf_4wide_math.hh"

struct Field
{
	Vec3i size;
	u32 count;
	Vec3 *dir;
	f32  *mag;
};

struct Pole
{
	Vec3 pos;
	f32 mag;
};

internal Field
create_field(Vec3i size)
{
	Field result = {};
	result.size = size;
	result.count = result.size.x*result.size.y*result.size.z;
	result.dir = (Vec3 *)calloc(result.count, sizeof(Vec3));
	result.mag = (f32 *)calloc(result.count, sizeof(f32));
	return result;
}

internal void
destroy_field(Field field)
{
	free(field.mag);
	free(field.dir);
}

internal void
update_field(Field field, Pole *poles, u32 pole_count)
{
	Vec3i field_pos;
	for(u32 field_index = 0;
			field_index < field.count;
			++field_index)
	{
		field_pos = vec3i(field_index % field.size.x,
						(field_index / field.size.x) % field.size.z,
						 field_index / (field.size.y * field.size.x)) - (field.size/2);
		field.mag[field_index] = 1;
		field.dir[field_index] = normalized(vec3(field_pos.x, field_pos.y, field_pos.z));
#if 0
	 	for(u32 pole_index = 0;
				pole_index < pole_count;
				++pole_index)
		{
		}
#endif
	}
}

Field field;
hdf_Instance_Mesh field_mesh;
hdf_Transform *field_transforms;
Mat4 *field_model_mat;
Mat3 *field_normal_mat;

void start()
{
	char *vert = hdf_load_text_file("Shaders/default_instance.vert");
	char *frag = hdf_load_text_file("Shaders/default.frag");
	GLuint shader = hdf_create_shader_program(vert, frag);
	free(frag);
	free(vert);

	//field_mesh = hdf_create_instance_mesh(hdf_Sprite_Mesh);

	field_mesh = hdf_create_instance_mesh(hdf_createMeshOBJ("Assets/quad.obj", shader));
	field_mesh.texture = hdf_load_texture_from_file("Assets/potato.png");
	hdf_Main_Camera.transform.position = vec3(0,0,18);
	hdf_Main_Camera.transform.rotation = euler_to_quat(vec3(0,0,0));

	field = create_field(Vec3i{10, 10, 10});
	update_field(field, NULL, 0);
	field_transforms = (hdf_Transform *)malloc(field.count * sizeof(hdf_Transform));

	field_model_mat = (Mat4*)malloc(field.count * sizeof(Mat4));
	field_normal_mat = (Mat3*)malloc(field.count * sizeof(Mat3));
}

char title[256];
void update_and_renderer(float delta)
{
	f32 delta_mod = delta * 1.0f;

	if(hdf_get_input(hdf_Input_Pressed, KEY_W))
		hdf_Main_Camera.transform.position.z -= delta * 20;
	else if(hdf_get_input(hdf_Input_Pressed, KEY_S))
		hdf_Main_Camera.transform.position.z += delta * 20;

	if(hdf_get_input(hdf_Input_Pressed, KEY_A))
		hdf_Main_Camera.transform.position.x -= delta * 20;
	else if(hdf_get_input(hdf_Input_Pressed, KEY_D))
		hdf_Main_Camera.transform.position.x += delta * 20;

	if(hdf_get_input(hdf_Input_Pressed, KEY_Space))
		hdf_Main_Camera.transform.position.y += delta * 20;
	else if(hdf_get_input(hdf_Input_Pressed, KEY_Shift))
		hdf_Main_Camera.transform.position.y -= delta * 20;

	for(u32 field_index = 0;
			field_index < field.count;
			++field_index)
	{
		Vec3 pos = vec3(field_index % field.size.x,
						(field_index / field.size.x) % field.size.z,
						 field_index / (field.size.y * field.size.x));
		field_transforms[field_index].position = (pos - vec3(5, 5, 5));
		field_transforms[field_index].rotation = (Vec4){field.dir[field_index] * 0.37f, 0.93f};
		field_transforms[field_index].scale = vec3(0.2, 0.2, 0.2);
	}
	
	//hdf_render_sprite(sprite, sprite_transform);
	hdf_render_mesh_instances(field_mesh, field_transforms, field_model_mat, field_normal_mat, field.count);
	
	sprintf(title,"%fms %f fps", delta, 1.0f/delta);
	glfwSetWindowTitle(hdf_Main_Window, title);
}

int main()
{
	hdf_init((Vec2i){1155, 650}, &start, &update_and_renderer);
	hdf_quit();
}
