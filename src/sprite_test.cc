#include "hdframework.hh"
#include "hdf_math.hh"
#include "hdf_4wide_math.hh"


hdf_Sprite sprite;
hdf_Transform transfom;

void start()
{
	sprite = hdf_create_sprite(hdf_Sprite_Shader_Program, 0); 
	sprite.texture = hdf_load_texture_from_file("Assets/potato2.png");
	transfom.position = vec3(0,0,0);
	transfom.scale = vec3(1,1,1);
	transfom.rotation = (Vec4){0,0,0,1};

	hdf_Main_Camera.transform.position = vec3(0,0,18);
	hdf_Main_Camera.transform.rotation = euler_to_quat(vec3(0,0,0));
}

void update_and_renderer(float delta)
{
	if(hdf_get_input(hdf_Input_Pressed, KEY_W))
		hdf_Main_Camera.transform.position.z -= delta * 20;
	else if(hdf_get_input(hdf_Input_Pressed, KEY_S))
		hdf_Main_Camera.transform.position.z += delta * 20;

	if(hdf_get_input(hdf_Input_Pressed, KEY_A))
		hdf_Main_Camera.transform.position.x -= delta * 20;
	else if(hdf_get_input(hdf_Input_Pressed, KEY_D))
		hdf_Main_Camera.transform.position.x += delta * 20;

	if(hdf_get_input(hdf_Input_Pressed, KEY_Space))
		hdf_Main_Camera.transform.position.y += delta * 20;
	else if(hdf_get_input(hdf_Input_Pressed, KEY_Shift))
		hdf_Main_Camera.transform.position.y -= delta * 20;
	
	//hdf_update_sprite(&sprite);
	hdf_render_sprite(sprite, transfom);
}

int main()
{
	hdf_init((Vec2i){1280, 720}, &start, &update_and_renderer);
	hdf_quit();
}
