#include "hdframework.hh"
#include "hdf_math.hh"
#include "hdf_4wide_math.hh"

hdf_Mesh plannet_mesh;
hdf_Mesh moon_mesh;

Vec3_4x moon_pos;
Vec3_4x moon_vel;

f32_4x zero4x;
f32_4x one4x;
f32_4x mass;

Vec4 plannet_rotation = {};

Vec3 potato2_vel;
Vec3 potato3_vel;

hdf_Audio audio;

#define mm_square(x) _mm_mul_ps((x),(x))

f64 init_time;

void start()
{
	hdf_load_audio(&audio, "Assets/omni3_loop.ogg");
	hdf_play_audio(audio);

	plannet_mesh = hdf_createMeshOBJ("Assets/Planet3.obj", hdf_Default_Shader_Program);
	plannet_mesh.texture = hdf_load_texture_from_file("Assets/PlanetTexture.png");

	moon_mesh = hdf_createMeshOBJ("Assets/Moon.obj", hdf_Default_Shader_Program);
	moon_mesh.texture = hdf_load_texture_from_file("Assets/MoonTexture.png");

	hdf_Main_Camera.transform.position = vec3(0,5,18);
	hdf_Main_Camera.transform.rotation = euler_to_quat(vec3(0.2,0,0));

	moon_pos.x = F32_4x(0.0f, 5.0f, 0.0f, -5.0f);
	moon_pos.z = F32_4x(0.0f, 0.0f, 0.0f, 0.0f);
	moon_pos.y = F32_4x(5.0f, 0.0f, -5.0f, 0.0f);

	moon_vel.x = F32_4x(0.0f, 0.0f, 0.0f, 0.0f);
	moon_vel.y = F32_4x(0.0f, 9.0f, 0.0f, -9.0f);
	moon_vel.z = F32_4x(9.0f, 0.0f, -9.0f, 0.0f);

	zero4x = F32_4x(0.0f);
	one4x = F32_4x(1.0f);
	mass = F32_4x(300.0f * 1.0f);

	init_time = glfwGetTime() - init_time;
	printf("Init time %lf\n", init_time);
}

void update_and_renderer(float delta)
{
	persistent float t = 0;
	persistent float rot = 0;
	f32 delta_mod = delta * 1.0f;

	f32_4x delta4x = F32_4x(delta_mod);
	f32_4x distance_sqr = length_squared(moon_pos);
	f32_4x acc = mass / distance_sqr;
	f32_4x distace = square_root(distance_sqr);

	moon_vel -= acc * (moon_pos / distace) * delta4x;
	moon_pos += moon_vel * delta4x;

	// TODO Use umpacking for this
	hdf_Transform transforms[4];
	for(int i = 0; i < 4; i++) {
		transforms[i] = {
			{moon_pos.ex[i],moon_pos.ey[i],moon_pos.ez[i]},
			{0.6f, 0.6f, 0.6f},
			{1.0f, 0.0f, 0.0f, 0.0f}
		};
	}

	Vec3 colors[4];	
	colors[0] = vec3(1,0,0);
	colors[1] = vec3(0,1,0);
	colors[2] = vec3(0,0,1);
	colors[3] = vec3(1,1,1);
	hdf_render_mesh_modulated(moon_mesh, transforms, colors, 4);

	if(hdf_get_input(hdf_Input_Pressed, KEY_W))
		hdf_Main_Camera.transform.position -= vec3(0,0,1);
	else if(hdf_get_input(hdf_Input_Pressed, KEY_S))
		hdf_Main_Camera.transform.position += vec3(0,0,1);

	t += delta_mod;
	plannet_rotation = (Vec4){normalized(vec3(0.5,1,0)) * sinf(t/2), cosf(t/2)};

	hdf_render_mesh(plannet_mesh, (hdf_Transform){vec3(0.0f,0.0f,0.0f), vec3(1.0f,1.0f,1.0f), plannet_rotation});
	printf("%f %f\r", delta, 1.0f/delta);
	fflush(stdout);
}

int main()
{
	hdf_init((Vec2i){1280, 720}, &start, &update_and_renderer);
	init_time = glfwGetTime();
	hdf_destroy_audio(audio);
	hdf_quit();
}
