#version 420 core

in vec2 uv;
in vec3 Color;

varying vec3 Normal;
out vec4 outColor;

uniform sampler2D tex;

void main()
{
	vec3 norm = normalize(Normal);
	vec3 lightDir = normalize(vec3(1,0,0));
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = (diff + 0.1) * vec3(1,1,1);

	outColor = texture(tex, uv) * vec4(diffuse, 1.0) * vec4(Color, 1.0);
}

