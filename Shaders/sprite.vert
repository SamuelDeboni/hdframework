#version 420 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 uvcord;

out vec3 Color;
out vec2 uv;
out vec4 ColorMod;

uniform mat4 modelViewMatrix;
uniform mat4 cameraMatrix;
uniform mat4 projectionMatrix;
uniform vec4 colorMod;

void main()
{
	uv = vec2(uvcord.x , -uvcord.y);
	vec4 pos = vec4(position , 1.0) * modelViewMatrix * cameraMatrix; 
	ColorMod = colorMod;
	gl_Position =  pos * projectionMatrix;
}
