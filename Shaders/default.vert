#version 420 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 uvcord;
layout (location = 2) in vec3 normal;

out vec3 Color;
out vec2 uv;

varying vec3 Normal;

uniform mat3 normalMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 cameraMatrix;
uniform mat4 projectionMatrix;
uniform vec3 colorMod;

void main()
{
	uv = vec2(uvcord.x , -uvcord.y);
	Normal = inverse(normalMatrix) * normal;
	Color = colorMod;
	mat4 M = modelViewMatrix * cameraMatrix * projectionMatrix;
	gl_Position =  vec4(position, 1.0) * M; 
}
