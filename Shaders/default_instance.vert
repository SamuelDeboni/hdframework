#version 420 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 uvcord;
layout (location = 2) in vec3 normal;
layout (location = 3) in mat4 modelViewMatrix;
layout (location = 7) in mat3 normalMatrix;

out vec3 Color;
out vec2 uv;
flat out int InstanceID;

varying vec3 Normal;

uniform mat4 projectionMatrix;
uniform mat4 cameraMatrix;
uniform vec3 colorMod;

void main()
{
	uv = vec2(uvcord.x , -uvcord.y);
	Normal = normalMatrix * normal;
	Color = colorMod;
	vec4 pos = vec4(position, 1.0) * modelViewMatrix * cameraMatrix; 
	gl_Position =  pos * projectionMatrix;
	InstanceID = gl_InstanceID;
}
